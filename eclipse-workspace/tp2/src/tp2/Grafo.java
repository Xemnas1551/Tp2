package tp2;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;

public class Grafo {
	
	private ArrayList< HashMap<Integer, Double>> vecinos; 
	private int tamanio;
	
	public Grafo(int vertices) {
		this.vecinos = new ArrayList< HashMap<Integer, Double>>();
		this.tamanio = vertices;
		inicializar();
	}
	
	public void inicializar() {
		for(int i = 0; i < tamanio(); i++)
			vecinos.add(new HashMap<Integer, Double>());
	}
	
	public int tamanio() {
		return this.tamanio;
	}
	   
	public void agregarArista(int i, int j, double peso) {
		if(i == j)
			throw new IllegalArgumentException("No se permiten loops");
		
		if(i<0 || i>= tamanio())
			throw new IllegalArgumentException("Vertice invalido " + i);
		
		if(j<0 || j>= tamanio())
			throw new IllegalArgumentException("Vertice invalido " + j);
		
		
		vecinos.get(i).put(j, peso);
		vecinos.get(j).put(i, peso);
	}
	
	public boolean existeArista(int i, int j) {
		return vecinos.get(i).containsKey(j);
	}
	
	public void eliminarArista(int i, int j) {
		if(i == j)
			throw new IllegalArgumentException("No es posible eliminar esta arista");
		
		if(!existeArista(i, j))
			throw new IllegalArgumentException("No existe la arista");
		
		vecinos.get(i).remove(j);
	}
	
	
	public static  Grafo  AplicarPrim(Grafo m) {
		Grafo p = new Grafo(m.tamanio());
		
		ArrayList<Integer> vertices= new ArrayList<Integer>();
		vertices.add(0);
		
		int i =0;
		while (i < m.tamanio()-1) {
			 
			Tupla<Integer,Integer> VerticesConAristaMenor= elegirAristaMenorDeVecinos(m,vertices);
			double PesoAristaElegida= m.pesoArista(VerticesConAristaMenor.getX(), VerticesConAristaMenor.getY());
			vertices.add(VerticesConAristaMenor.getY());
			p.agregarArista(VerticesConAristaMenor.getX(), VerticesConAristaMenor.getY(), PesoAristaElegida);
			i=i+1;
			
			
		
		
		}
		return p;
			
	}



	private static Tupla<Integer, Integer> elegirAristaMenorDeVecinos(Grafo m, ArrayList<Integer> n) {
		Tupla<Integer,Integer> Vertices = new Tupla<Integer,Integer>(0,1);
		double minimo= Integer.MAX_VALUE;
		
		for(int i =0; i < n.size();i++) {
			for(int j: m.vecinos(n.get(i))) {
				
				if(!n.contains(j)) {
					if(m.pesoArista(n.get(i), j)<minimo) {
						Vertices.setX(n.get(i));
						Vertices.setY(j);
						minimo=m.pesoArista(n.get(i), j);
					}
				}
			}
		}
		
		
		return Vertices;
	}
	
	
	
	
	
	
	
	public Set<Integer> vecinos(int i) {
		return vecinos.get(i).keySet();
	}
	
	public double pesoArista(int i, int j) {
		return vecinos.get(i).get(j);
	}

}