package tp2;
import static org.junit.Assert.*;

import org.junit.Test;

public class PrimTest {

	@Test
	public void test() {
		Grafo m= new Grafo(9);
		m.agregarArista(0, 6, 6);
		m.agregarArista(0, 7, 3);
		m.agregarArista(0, 3, 10);
		m.agregarArista(6, 1, 12);
		m.agregarArista(6, 4, 1);
		m.agregarArista(7, 4, 0);
		m.agregarArista(3, 2, 3);
		m.agregarArista(4, 1, 15);
		m.agregarArista(4, 2, 7);
		m.agregarArista(1, 8, 2);
		m.agregarArista(2, 8, 9);
		m.agregarArista(5, 1, 5);
		m.agregarArista(5, 3, 10);
		
		Grafo p = Grafo.AplicarPrim(m);
		
		assertEquals(1,p.vecinos(0).size());
		assertEquals(3,p.vecinos(2).size());
		assertEquals(3,p.vecinos(4).size());
		assertEquals(2,p.vecinos(1).size());
		assertEquals(2,p.vecinos(8).size());
		
		
	
		

		
		
		
	}

}
