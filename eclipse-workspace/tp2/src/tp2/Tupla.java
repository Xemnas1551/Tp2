package tp2;

public class Tupla<S1, S2> {
	S1 elemento1;
	S2 elemento2;
	
	public Tupla(S1 x , S2 y){
		elemento1=x;
		elemento2=y;
	}


	public S1 getX() {
		return elemento1;
	}
	public S2 getY() {
		return elemento2;
	}

	public void setX(S1 n) {
		this.elemento1=n;
	}
	public void setY(S2 n) {
		this.elemento2=n;
	}

}
